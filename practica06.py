import unittest

class Empleado:
    def __int__(self, n, s):
        self.nombre = n
        self.nomina = s

#cambio de la función usando la clase para simplificar
#self no es un parámetro, solo hace referencia a la clase de emlpeado que hemos creado
#CalculoImpuesto es un método porque stá dentro de la clase Empleado y hace referencia a esta mediante self
    def calcula_impuesto(self):
        return 0.3*self.nomina

#creamos un método que devuelve el string correspondiente a cada nombre e impuesto
#el objetivo de esto es no incluir ningún print dentro de la clase
#cambiamos el nombre de calcula_impuesto porque en lo métodos se suelen usar minúsculas y en forma de orden
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=tmp)

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.nomina, 5000)

    def test_impuestos(self):
        el = Empleado("nombre", 5000)
        self.assertEqual(el.nomina, 1500)

    def test_str(self):
        el = Empleado("Pepe", 50000)
        self.assertEqual("El empleado Pepe debe pagar 15000.00", el.__str__())

if __name__ == "__main__":
    unittest.main()