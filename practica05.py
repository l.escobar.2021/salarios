
class Empleado:
    def __int__(self, n, s):
        self.nombre = n
        self.nomina = s

#cambio de la función usando la clase para simplificar
#self no es un parámetro, solo hace referencia a la clase de emlpeado que hemos creado
#CalculoImpuesto es un método porque stá dentro de la clase Empleado y hace referencia a esta mediante self
    def calcula_impuesto(self):
        return 0.3*self.nomina

#creamos un método que devuelve el string correspondiente a cada nombre e impuesto
#el objetivo de esto es no incluir ningún print dentro de la clase
#cambiamos el nombre de calcula_impuesto porque en lo métodos se suelen usar minúsculas y en forma de orden
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=tmp)

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)

#caculo el total de los impuestos de Pepe y Ana con una variable total llamando al método Calculo_Impuesto
total = empleadoPepe.calcula_impuesto() + empleadaAna.calcula_impuesto()

#llamo al método llamado imprime para sacar los impuestos de Ana y Pepe
empleadoPepe.imprime()
empleadaAna.imprime()

#imprimo el total de impuestos
print("Los impuestos a pagar en total son {:.2f} euros".format(total))

#sinónimo de instancia=objeto