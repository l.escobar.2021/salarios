def calculo_coste_salarial(nomina, impuestos):
    coste_salarial = nomina + impuestos
    return coste_salarial

class Empleado:
    def __int__(self, n, s):
        self.nombre = n
        self.nomina = s
#cambio de la función usando la clase para simplificar
#self no es un parámetro, solo hace referencia a la clase de emlpeado que hemos creado
#CalculoImpuesto es un método porque stá dentro de la clase Empleado y hace referencia a esta mediante self
    def Calculo_Impuesto(self):
        impuesto = 0.3*self.nomina
        return impuesto
#añadimos otro métdo que imprime los impuestos de cada empleado
    def imprime(self):
        tmp = self.Calculo_Impuesto()
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=tmp)))

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)
#caculo el total de los impuestos de Pepe y Ana con una variable total llamando al método Calculo_Impuesto
total = empleadoPepe.Calculo_Impuesto() + empleadaAna.Calculo_Impuesto()
#llamo al método llamado imprime para sacar los impuestos de Ana y Pepe
empleadoPepe.imprime()
empleadaAna.imprime()
#imprimo el total de impuestos
print("Los impuestos a pagar en total son {:.2f} euros".format(total))