# Salarios
#mi código original
def calculo_impuesto(nomina):
    impuesto = 0.3*nomina
    return impuesto

def calculo_coste_salarial(nomina, impuestos):
    coste_salarial = nomina + impuestos
    return coste_salarial

dict = {"Ana": 30000, "Pepe": 20000, "Julia": 17000, "Marcos": 25000, "Andrea": 37000, "Pedro": 14000, "Carmen": 48000, "Alberto": 36000, "Lidia": 50000, "Sandra": 27000}

for empleado in dict:
    print("El impuesto de", empleado, "es", calculo_impuesto(dict[empleado]), "y su coste salarial es", calculo_coste_salarial(dict[empleado], calculo_impuesto(dict[empleado])))
    