class Empleado:

    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calcula_impuesto(self):
        return 0.3*self.nomina

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuesto())