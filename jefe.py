class Empleado:

    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calcula_impuesto(self):
        return 0.3*self.nomina

    def __str__(self):
        return 0.3*self.nomina

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuesto())


class Jefe(Empleado):
    def __init__(self, n, s, extra=0):
        super().__init__(n, s)   #le indica al padre que se construya con los elementos que él tiene
        self.bonus = extra

    def calcula_impuesto2(self):
        return 0.3* (self.nomina + self.bonus)

    #def calcula_impuesto2(self):
            #return super().calcula_impuestos() + self.bonus*0,3
            #esta foma es más eficiente porque dejo a mi padre que haga lo que tiene que hacer, y luego yo incluyo lo mío
            #además, si tengo que cambiar algo en algún momento, de esta forma sólo tendría que hacerlo en la clase de padre(empleado)

    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuesto())


e1 = Empleado("Pepe", 20000)
j1 = Jefe("Ana", 30000, 2000)

    