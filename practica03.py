
def calculo_coste_salarial(nomina, impuestos):
    coste_salarial = nomina + impuestos
    return coste_salarial

class Empleado:
    def __int__(self, n, s):
        self.nombre = n
        self.nomina = s
#cambio de la función usando la clase para simplificar
#self no es un parámetro, solo hace referencia a la clase de emlpeado que hemos creado
#CalculoImpuesto es un étodo porque stá denro de la clase Empleado y hace referencia a esta mediante self
    def Calculo_Impuesto(self):
        impuesto = 0.3*self.nomina
        print ("El empleado {name} debe pagar {tax:.f}".format(name=nombre (tax=impuesto)))
        return impuesto

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana", 30000)
#uso de la clase para la resolución final del problema
calculo_coste_salarial(empleadoPepe.CalculoImpuesto()+ empleadaAna.CalculoImpuesto())